const mocha = require('mocha');
const sinon = require('sinon');
const chai = require('chai');
const chaiNock = require('chai-nock');
const chaiHttp = require('chai-http');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire').noCallThru();

const { expect } = chai;
chai.use(chaiHttp);
chai.use(sinonChai);
chai.use(chaiNock);


const redisClientStub = {
  addItem: sinon.stub(),
  getItems: sinon.stub(),
  removeItem: sinon.stub(),
};

const axiosStub = {
  create: sinon.stub(),
  post: sinon.stub(),
  get: sinon.stub()
}
const app = proxyquire('../src', {
  'axios': axiosStub,
  './redisClient': redisClientStub,
});

describe('webhook-api', () => {
    describe('expect to return bad request', () => {
      it('when the provider is not supported', async () => {
        const res = await chai.request('localhost:3001')
          .post('/providers')
          .set('content-type', 'application/json')
          .send({ provider: 'random' });

        expect(res.status).to.equal(400);
      })

      it('when the callbackUrl is not valid', async () => {
        const res = await chai.request('localhost:3001')
        .post('/providers')
        .set('content-type', 'application/json')
        .send({ provider: 'gas', callbackUrl: '@test' });

        expect(res.status).to.equal(400);

      });
    });

    describe('expect to return a success response', () => {
      beforeEach(() => {
        axiosStub.get.reset();
        axiosStub.post.reset();
        redisClientStub.getItems.reset();
        redisClientStub.addItem.reset();
      });

      it('and send the request to the callback url ', async () => {
        const provider = 'gas';
        const callbackUrl = 'http://callback-url';
        const responseData = {
            "billedOn": "2020-04-07T15:03:14.257Z",
            "amount": 22.27
        };

        // stubs
        axiosStub.get = sinon.stub().returns({
          data: responseData
        });
        redisClientStub.getItems = sinon.stub().returns([]);

        // actual call
        const res = await chai.request('http://localhost:3001')
          .post('/providers')
          .set('content-type', 'application/json')
          .send({ provider, callbackUrl });

          expect(res.status).to.equal(202);
          expect(axiosStub.get).to.have.been.calledWith(`http://localhost:3000/providers/${provider}`);
          expect(axiosStub.post).to.have.been.calledWith(callbackUrl, responseData);
      });

      it('and store the callback url until the providers endpoint returns fresh data', async () => {
        const provider = 'gas';
        const callbackUrl = 'http://callback-url';
        const responseData = {
            "billedOn": "2020-04-07T15:03:14.257Z",
            "amount": 22.27
        };
        // stubs
        axiosStub.get = sinon.stub().throws({ response: { status: 500 }});
        redisClientStub.getItems = sinon.stub().returns([]);

        // actual call
        const res = await chai.request('http://localhost:3001')
          .post('/providers')
          .set('content-type', 'application/json')
          .send({ provider, callbackUrl });

        expect(res.status).to.equal(202);
        expect(redisClientStub.addItem).to.have.been.calledWith(provider, [callbackUrl]);
      });

      it('and deliver the data to all the callbacks that failed', async () => {
        const provider = 'gas';
        const failedCallbacks = ['http://callback-url'];
        const callbackUrl = 'http://callback-url-new';
        const responseData = {
            "billedOn": "2020-04-07T15:03:14.257Z",
            "amount": 22.27
        };
        // stubs
        axiosStub.get = sinon.stub().onFirstCall().throws({ response: { status: 500 }});
        axiosStub.get = sinon.stub().onSecondCall().returns(responseData);
        redisClientStub.getItems = sinon.stub().returns(failedCallbacks);

        // actual call
        const res = await chai.request('http://localhost:3001')
          .post('/providers')
          .set('content-type', 'application/json')
          .send({ provider, callbackUrl });

        await chai.request('http://localhost:3001')
          .post('/providers')
          .set('content-type', 'application/json')
          .send({ provider, callbackUrl });

        // expectations
        expect(res.status).to.equal(202);
        failedCallbacks.push(callbackUrl);
        expect(redisClientStub.addItem).to.have.been.calledWith(provider, failedCallbacks);
        expect(axiosStub.post).to.have.been.calledTwice;
      });
    });
});
