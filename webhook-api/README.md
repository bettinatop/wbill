# README #

### What is this repository for? ###

WonderBill - A fault-tolerant approach when integrations with external APIs might fail

### How do I get set up? ###
The repo contains two directories. The datahog and the webhook-api.
The datahog is exactly the same repo provided by WonderBill to simulate requests sent to external services, whereas the webhook-api directory contains the solution.


* How to use the app
```
cd webhook-api
./build.sh
```

* How to send a request to the app

`curl -XPOST localhost:3001/providers -d '{"provider": "gas", "callbackUrl": "http://localhost:3001/callback/333"}' -H "content-type: application/json"`


 
* How to run tests
```
cd webhook-api
yarn && yarn test
```

## Tech

Dillinger uses a number of open source projects to work properly:

- [Docker] - for creating containers
- [Node] - for creating an http server
- [Redis] - for storing data


#### TODOs

- [ ] Advanced handling of errors
- [ ] Introduce queues for temp errors returned by the external services
- [ ] Centralise configuration
