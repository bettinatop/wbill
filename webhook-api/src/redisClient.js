const { promisify } = require('util');
const redis = require('redis');
const log = require('./logger');

/**
 * Redis Client wrapper
 * promisifies get, set and del functions
 * adds a prefix in front of every key stored
 * @constructor
 */
class RedisClient {
  constructor(prefix = '') {
    this.prefix = prefix;
    let connection;
    if (process.env.NODE_ENV === 'development') {
      connection = { host: 'localhost' }
    } else {
      connection = { host: 'redis' }
    }
    this.redisClient = redis.createClient(connection);
    this.getAsync = promisify(this.redisClient.get).bind(this.redisClient);
    this.getItemsAsync = promisify(this.redisClient.smembers).bind(this.redisClient);
    this.setAsync = promisify(this.redisClient.set).bind(this.redisClient);
    this.pushItemAsync = promisify(this.redisClient.rpush).bind(this.redisClient);
    this.addItemAsync = promisify(this.redisClient.sadd).bind(this.redisClient);
    this.delAsync = promisify(this.redisClient.del).bind(this.redisClient);
    this.removeItemAsync = promisify(this.redisClient.srem).bind(this.redisClient);
    this.setListeners();
  }

  setListeners() {
    this.redisClient.on('error', (err) => {
      log.info('[Redis:error] ' + err);
    });
    this.redisClient.on('connect', () => log.info('[Redis]: connected'));
    this.redisClient.on('ready', () => log.info('[Redis]: ready'));
  }

  get(key) {
    return this.getAsync(this.prefix + key);
  }

  set(key, data, ...options) {
    return this.setAsync(this.prefix + key, data, ...options);
  }

  addItem(key, data, ...options) {
    return this.addItemAsync(this.prefix + key, data, ...options);
  }

  getItems(key) {
    return this.getItemsAsync(this.prefix + key);
  }

  delete(key) {
    return this.delAsync(this.prefix + key);
  }

  removeItem(key, value) {
    return this.removeItemAsync(this.prefix + key, value);
  }
}
module.exports = new RedisClient();
