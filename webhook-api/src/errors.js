/**
 * Defines the possible errors returned when interacting with external services
 * @module Errors
 */
const Errors = {
  CLIENT: 'client-error',
  BAD_GATEWAY: 'retry',
  SERVICE_UNAIVALABLE: 'service-error',
};

module.exports = Errors;
