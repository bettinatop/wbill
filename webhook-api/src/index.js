const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
// const queue = require('./queueConnector');
const redis = require('./redisClient');
const logger = require('./logger');
const Errors = require('./errors');
const app = express();

axios.create({
  timeout: 3000,
})
app.use(bodyParser.json());

const SUPPORTED_PROVIDERS = ['internet', 'gas'];
const PROVIDERS_DOMAIN = (process.env.NODE_ENV === 'development') ? 'http://localhost:3000' : 'http://external-api:3000';
const URL_REGEX = new RegExp('^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$');

const savePendingCallbacks = async (provider, callbackUrl) => {
  const callbacks = await redis.getItems(provider) || [];
  callbacks.push(callbackUrl);
  return redis.addItem(provider, callbacks);
}

const getPendingCallbacks = (provider) => {
  return redis.getItems(provider);
}

const removeCallback = (provider, value) => {
  return redis.removeItem(provider, value);
}

const fetchProvidersData = async (provider) => {
  try {
    const { data } = await axios.get(`${PROVIDERS_DOMAIN}/providers/${provider}`);
    return data;
  } catch (e) {
    logger.error(`[fetchProvidersData]: ${e.response.status} - ${e.message}` );
    if (e.response.status >= 400 && e.response.status < 499) {
      throw new Error(Errors.CLIENT);
    }
    if (e.response.status === 502) {
      // we could possibly retry after X mins for Y times
      throw new Error(Errors.BAD_GATEWAY);
    }
    throw new Error(Errors.SERVICE_UNAIVALABLE);
  }
}

const sendRequest = (url, data) => {
  return axios.post(url, data);
}

const resolveCallback = async (provider, callbackUrl, data) => {
  logger.debug(`[resolveCallback]: for ${callbackUrl}`);
  try {
    await sendRequest(callbackUrl, data);
    await removeCallback(provider, callbackUrl)
  } catch (e) {
    logger.error('[resolveCallback]: ', e);
    throw e;
  }
}

app.post('/providers', async (req, res) => {
    const { provider, callbackUrl } = req.body;
    if (!SUPPORTED_PROVIDERS.includes(provider) || !URL_REGEX.test(callbackUrl)) {
      return res.sendStatus(400);
    }
    res.sendStatus(202);

    try {
      let providersData = await fetchProvidersData(provider);
      logger.debug('data fetched');

      const pendingCallbacks = await getPendingCallbacks(provider);
      logger.debug('pendingCallbacks', pendingCallbacks);

      let callbackFound = false;
      pendingCallbacks.map(async (cb) => {
        if (cb === callbackUrl) {
          callbackFound = true;
        }
        await resolveCallback(provider, cb, providersData);
      })
      if (!callbackFound) {
        await sendRequest(callbackUrl, providersData);
      }
    } catch (e) {
      logger.error(e.message)
      if (e.message !== Errors.CLIENT) {
        // IMPROVEMENT: handle this error
        await savePendingCallbacks(provider, callbackUrl);
      }
    }
});

app.post('/callback/:param', (req, res) => {
  logger.info(`callback data received ${req.params} ${JSON.stringify(req.body)}`);
  return res.sendStatus(200);
});

app.listen(3001, () => console.log(`listening at http://localhost:${3001}`));
